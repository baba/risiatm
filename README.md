# RisiATM

RisiATM est un client de bureau pour Risibank.
Il permet de chercher des stickers et enregistrer ses favoris dans son RisiWallet. 
La recherche de stickers est utilisable sans souris.

![screenshot](screenshot.png "Logiciel RisiATM")
