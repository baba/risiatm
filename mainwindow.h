#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>

typedef QSet <QString> RisiList;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    static QString * htmlFromStickers(RisiList & urls);

private slots:
    void on_recherche_returnPressed();
    void slotReadyRead();
    void slotLinkClicked(const QUrl & link);
    void on_btnVoirWallet_clicked();

    void on_btnModifWallet_clicked();

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager qnam;
    QNetworkReply *reply;
    RisiList wallet;
    bool dansWallet = false;

    const QString risiFilename = ".risiwallet";

    void chargerWallet();
    void sauvegarderWallet();
    void message(QString & msg);

};

#endif // MAINWINDOW_H
