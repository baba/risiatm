#include <QtNetwork>
#include <QUrl>
#include <iostream>
#include <QStyle>
#include <QDesktopWidget>
#include <QScreen>
#include <QStandardPaths>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->recherche->setFocus();
    connect(ui->webView, SIGNAL(linkClicked(const QUrl &)), this, SLOT(slotLinkClicked(const QUrl &)));
    ui->webView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks); //pour les liens
    move(QApplication::desktop()->screen()->rect().center()- frameGeometry().center()); //centrage de fenetre
    setWindowIcon(QIcon(":/embabouine.png"));
    ui->label->setOpenExternalLinks(true);

    chargerWallet();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_recherche_returnPressed()
{
    dansWallet = false;
    ui->btnModifWallet->setText("Ajouter au Risiwallet");

    QUrl url ("https://api.risibank.fr/api/v0/search");
    reply = qnam.post(QNetworkRequest (url), QByteArray::fromStdString(("search=" + ui->recherche->text().toStdString() + "")));
    connect(reply, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));

}

void MainWindow::slotReadyRead()
{
    RisiList urls;
    QString rawReply;
    QString *html;
    rawReply = reply->readAll();
    rawReply.replace("\\/","/"); //parce quia des trucs mochent

    int j = 0;
    int k = 0;
    while ((j = rawReply.indexOf("http", j)) != -1) {
        k = j; // le debuent
        j = rawReply.indexOf("\"",j);
        std::cout << rawReply.mid(k, j - k).toStdString() << std::endl;
        urls.insert(rawReply.mid(k, j - k));
    }
    if (urls.size() != 0)
        html = htmlFromStickers(urls);
    else
        html = new QString("<h1>[ALERTE]</h1><p>Aucun sticker trouvé</p>");

    ui->webView->setHtml(*html);
    free(html);
    ui->webView->setFocus();
}

void MainWindow::slotLinkClicked(const QUrl & link)
{
    std::cout << "uiuiui" << std::endl;
    ui->url->setText(link.toString());
    ui->url->selectAll();
    ui->url->setFocus();
}

void MainWindow::chargerWallet ()
{
    //wallet.insert("http://image.noelshack.com/fichiers/2017/18/1493933263-fou-rire-jesus.png");
    //wallet.insert("http://image.noelshack.com/fichiers/2017/18/1494194269-risibouchecinq.png");
    //wallet.insert("http://image.noelshack.com/fichiers/2018/34/4/1535040519-1529680164-risiboulbepetit.png");

    QFile f (QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/" + risiFilename);
    if (f.open(QIODevice::ReadOnly)) {
       QTextStream in(&f);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          wallet.insert(line);
       }
       f.close();
    }
}

void MainWindow::sauvegarderWallet ()
{
    std::cout << QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation).toStdString() + "/" + risiFilename.toStdString();
    QFile f (QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/" + risiFilename);
    if (f.open(QIODevice::WriteOnly)) {
        for (QString s : wallet) {
            f.write(s.toStdString().c_str());
            f.write("\n");
        }
        f.close();
    }

}


void MainWindow::on_btnModifWallet_clicked()
{
    QString msg;
    if (dansWallet) {
        wallet.remove(ui->url->text());
        msg = "Sticker supprimé";
    } else {
        wallet.insert(ui->url->text());
        msg = "Sticker ajouté";
    }
    sauvegarderWallet();
    message(msg);
}

void MainWindow::on_btnVoirWallet_clicked()
{
    dansWallet = true;
    ui->btnModifWallet->setText("Enlever du Risiwallet");
    QString* html;
    html = htmlFromStickers(wallet);
    ui->webView->setHtml(*html);
    ui->webView->setFocus();
    free(html);

}

 QString * MainWindow::htmlFromStickers(RisiList & urls)
 {
     QString* html = new  QString("<head><style> img { max-width: 104px; max-height: 78px; } </style></head>");
     for (QString s : urls) {
        *html += "<a href=\"" + s + "\"><img  src=\"" + s + "\" onerror=\"this.style.display='none'\"></a>";
     }
     return html;
}

 void MainWindow::message(QString & msg)
 {
     ui->lblmessage->setText(msg);
 }

